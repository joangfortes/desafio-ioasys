import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget customTextField(TextEditingController textEditingController,
    {bool obscureText = false,
    bool areCredentialsWrong = false,
    BuildContext context}) {
  return Container(
    padding: EdgeInsets.only(left: 20, top: 5, bottom: 5),
    decoration: BoxDecoration(
      color: Colors.grey.withOpacity(0.2),
      borderRadius: BorderRadius.all(
        Radius.circular(10),
      ),
      border: Border.all(
          color:
              areCredentialsWrong ? Colors.red : Colors.grey.withOpacity(0.2)),
    ),
    child: TextField(
      obscureText: obscureText,
      onTap: () {
        areCredentialsWrong = false;
      },
      decoration: InputDecoration(
        suffixIcon: areCredentialsWrong
            ? IconButton(
                onPressed: textEditingController.clear,
                icon: Icon(
                  Icons.clear,
                  color: Colors.red,
                ),
              )
            : null,
        border: InputBorder.none,
      ),
      controller: textEditingController,
    ),
  );
}
