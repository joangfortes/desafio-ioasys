import 'package:desafio_ioasys/config/size.config.dart';
import 'package:desafio_ioasys/models/enterprise.dto.dart';
import 'package:desafio_ioasys/util/constraints.dart';
import 'package:desafio_ioasys/views/enterprise_detail.view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Widget enterpriseCard({EnterpriseDto enterpriseDto, int index}) {
  List<Color> colors = [
    Colors.lightBlue,
    Colors.orangeAccent,
    Colors.green[800],
    Colors.deepPurpleAccent[400],
    Colors.amberAccent[200]
  ];
  return GestureDetector(
    child: Center(
      child: Container(
        height: SizeConfig.screenHeight / 6.0,
        decoration: BoxDecoration(color: colors[index % 5]),
        child: Center(
          child: Text(
            enterpriseDto.enterpriseName.toUpperCase(),
            style: TextStyle(fontSize: fontSizeBig, color: Colors.white),
          ),
        ),
      ),
    ),
    onTap: () {
      Get.to(EnterpriseDetailScreen(enterpriseDto, colors[index % 3]));
    },
  );
}
