import 'package:desafio_ioasys/controllers/enterprise.controller.dart';
import 'package:desafio_ioasys/models/enterprise.dto.dart';

class EnterprisesService {
  static Future<List<EnterpriseDto>> getEnterprises({String name}) async {
    List<EnterpriseDto> enterpriseList = [];

    await EnterprisesController.getEnterprises(name: name).then((response) {
      List<dynamic> list;
      list = response.data["enterprises"];

      for (Map<String, dynamic> json in list) {
        enterpriseList.add(EnterpriseDto.fromJson(json));
      }
    });
    return enterpriseList;
  }
}
