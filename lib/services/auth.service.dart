import 'package:desafio_ioasys/config/server.config.dart';
import 'package:desafio_ioasys/controllers/auth.controller.dart';
import 'package:desafio_ioasys/models/login.dto.dart';
import 'package:desafio_ioasys/models/user.dto.dart';
import 'package:desafio_ioasys/views/home.view.dart';
import 'package:desafio_ioasys/views/login_screen.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';

class AuthServices {
  Future<bool> makeLogin(TextEditingController emailTextEditingController,
      TextEditingController passwordTextEditingController,
      {bool areCredentialsWrong}) async {
    LoginDto loginDto = LoginDto(
        emailTextEditingController.text, passwordTextEditingController.text);
    await AuthController.login(loginDto).then((response) {
      switch (response.statusCode) {
        case HttpStatus.ok:
          List<String> headers = response.headers.toString().split('\n');
          ServerConfig.accessToken = headers[8].split(' ')[1];
          ServerConfig.uid = headers[14].split(' ')[1];
          ServerConfig.client = headers[17].split(' ')[1];
          loggedUserDto = UserDto.fromJson(response.data);
          areCredentialsWrong = false;
          Get.offAll(HomeScreen());
          break;
        default:
          areCredentialsWrong = true;
          break;
      }
    });
    return areCredentialsWrong;
  }
}
