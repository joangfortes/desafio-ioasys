import 'package:desafio_ioasys/config/server.config.dart';
import 'package:desafio_ioasys/models/login.dto.dart';
import 'package:dio/dio.dart';

class AuthController {
  static String endpoint = "users/auth/";

  static Future<Response> login(LoginDto loginDto) {
    String url = "${ServerConfig.apiUrl}/$endpoint/sign_in";

    Dio dio = Dio();
    return dio.post(url,
        data: loginDto.toJson(),
        options: Options(
            contentType: Headers.jsonContentType,
            responseType: ResponseType.json,
            validateStatus: (_) => true));
  }
}
