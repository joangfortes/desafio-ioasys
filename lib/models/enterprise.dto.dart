import 'package:desafio_ioasys/models/enterprise_type.dto.dart';

class EnterpriseDto {
  int id;
  String email;
  String facebook;
  String twitter;
  String linkedin;
  String phone;
  bool ownEnterprise;
  String enterpriseName;
  String photo;
  String description;
  String city;
  String country;
  int value;
  double sharePrice;
  EnterpriseTypeDto enterpriseTypeDto;

  EnterpriseDto.fromJson(Map<String, dynamic> json) {
    id = int.tryParse(json["id"].toString()) ?? 0;
    email = json["email_enterprise"];
    facebook = json["facebook"];
    twitter = json["twitter"];
    linkedin = json["linkedin"];
    phone = json["phone"];
    ownEnterprise = json["own_enterprise"];
    enterpriseName = json["enterprise_name"];
    photo = json["photo"];
    description = json["description"];
    city = json["city"];
    country = json["country"];
    value = int.tryParse(json["value"].toString()) ?? 0;
    sharePrice = double.tryParse(json["share_price"].toString()) ?? 0.0;
    enterpriseTypeDto = json["enterprise_type"] != null
        ? EnterpriseTypeDto.fromJson(json["enterprise_type"])
        : null;
  }
}
