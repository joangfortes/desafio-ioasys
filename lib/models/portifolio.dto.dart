import 'package:desafio_ioasys/models/enterprise.dto.dart';

class PortfolioDto {
  int enterprisesNumber;
  List<EnterpriseDto> enterprises;

  PortfolioDto.fromJson(Map<String, dynamic> json) {
    enterprisesNumber =
        int.tryParse(json["enterprises_number"].toString()) ?? 0;

    enterprises = [];

    for (Map<String, dynamic> _json in json["enterprises"]) {
      if (_json != null) {
        enterprises.add(EnterpriseDto.fromJson(_json));
      }
    }
  }
}
