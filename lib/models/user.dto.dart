import 'package:desafio_ioasys/models/enterprise.dto.dart';
import 'package:desafio_ioasys/models/investor.dto.dart';

class UserDto {
  InvestorDto investorDto;
  EnterpriseDto enterpriseDto;

  UserDto.fromJson(Map<String, dynamic> json) {
    investorDto = json["investor"] != null
        ? InvestorDto.fromJson(json["investor"])
        : null;
    enterpriseDto = json["enterprise"] != null
        ? EnterpriseDto.fromJson(json["enterprise"])
        : null;
  }
}
