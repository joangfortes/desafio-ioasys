import 'package:desafio_ioasys/models/portifolio.dto.dart';

class InvestorDto {
  int id;
  String name;
  String email;
  String city;
  String country;
  double balance;
  String photo;
  PortfolioDto portfolioDto;
  double portfolioValue;
  bool firstAccess;
  bool superAngel;

  InvestorDto.fromJson(Map<String, dynamic> json) {
    id = int.tryParse(json["id"].toString()) ?? 0;
    name = json["investor_name"];
    email = json["email"];
    city = json["city"];
    country = json["country"];
    balance = double.tryParse(json["balance"].toString()) ?? 0.0;
    photo = json["photo"];
    portfolioDto = json["portfolio"] != null
        ? PortfolioDto.fromJson(json["portfolio"])
        : null;
    portfolioValue = json["portfolio_value"];
    firstAccess = json["first_access"];
    superAngel = json["super_angel"];
  }
}
