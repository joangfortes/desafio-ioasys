import 'package:desafio_ioasys/config/size.config.dart';
import 'package:desafio_ioasys/models/enterprise.dto.dart';
import 'package:desafio_ioasys/util/constraints.dart';
import 'package:flutter/material.dart';

class EnterpriseDetailScreen extends StatefulWidget {
  final EnterpriseDto enterpriseDto;
  final Color color;

  EnterpriseDetailScreen(this.enterpriseDto, this.color) : super();

  @override
  _EnterpriseDetailScreenState createState() =>
      _EnterpriseDetailScreenState(this.enterpriseDto, this.color);
}

class _EnterpriseDetailScreenState extends State<EnterpriseDetailScreen> {
  final EnterpriseDto enterpriseDto;
  final Color color;

  _EnterpriseDetailScreenState(this.enterpriseDto, this.color);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding:
                    EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 30),
                child: Row(
                  children: [
                    Flexible(
                      child: GestureDetector(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.all(
                              Radius.circular(5),
                            ),
                          ),
                          height: SizeConfig.screenWidth / 10.0,
                          child: Center(
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.red,
                              size: 25,
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                      flex: 1,
                    ),
                    Flexible(
                      child: Container(
                        height: SizeConfig.screenWidth / 10.0,
                        child: Center(
                          child: Text(
                            enterpriseDto.enterpriseName.toUpperCase(),
                            style: TextStyle(
                                fontSize: fontSizeBig,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      flex: 9,
                    ),
                    Flexible(
                      child: Container(
                        height: SizeConfig.screenWidth / 10.0,
                      ),
                      flex: 1,
                    ),
                  ],
                ),
              ),
              Center(
                child: Container(
                  height: SizeConfig.screenHeight / 6.0,
                  decoration: BoxDecoration(color: color),
                  child: Center(
                    child: Text(
                      enterpriseDto.enterpriseName.toUpperCase(),
                      style:
                          TextStyle(fontSize: fontSizeBig, color: Colors.white),
                    ),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 30)),
              Padding(
                padding:
                    EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 30),
                child: Text(
                  enterpriseDto.description,
                  style: TextStyle(fontSize: fontSizeBig),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
