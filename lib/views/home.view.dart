import 'package:desafio_ioasys/components/home_screen_components.dart';
import 'package:desafio_ioasys/config/size.config.dart';
import 'package:desafio_ioasys/models/enterprise.dto.dart';
import 'package:desafio_ioasys/services/enterprises.service.dart';
import 'package:desafio_ioasys/util/constraints.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen() : super();

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController searchTextEditingController;

  bool isLoading = false;

  List<EnterpriseDto> enterprises = [];

  void initState() {
    super.initState();
    if (mounted) {
      setState(() {
        isLoading = true;
      });
    }
    EnterprisesService.getEnterprises().then((_enterprises) {
      for (EnterpriseDto enterpriseDto in _enterprises) {
        enterprises.add(enterpriseDto);
      }
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  stops: [
                    0.1,
                    0.3,
                    0.8,
                  ],
                  colors: [
                    Colors.redAccent[400],
                    Colors.pinkAccent[700],
                    Colors.deepPurple,
                  ],
                ),
              ),
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [],
                ),
                width: SizeConfig.screenWidth,
                height: (MediaQuery.of(context).viewInsets.bottom == 0)
                    ? SizeConfig.screenHeight / 3.0
                    : SizeConfig.screenHeight / 5.0,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 30, left: 20, right: 20),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 20, top: 5, bottom: 5),
                    decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(0.4),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    child: TextField(
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.search),
                        border: InputBorder.none,
                      ),
                      controller: searchTextEditingController,
                      onChanged: (value) {
                        setState(() {
                          if (mounted) {
                            isLoading = true;
                          }
                          EnterprisesService.getEnterprises(name: value)
                              .then((_enterprises) {
                            enterprises.clear();
                            for (EnterpriseDto enterpriseDto in _enterprises) {
                              enterprises.add(enterpriseDto);
                            }
                            if (mounted) {
                              setState(() {
                                isLoading = false;
                              });
                            }
                          });
                        });
                      },
                    ),
                  ),
                  isLoading
                      ? Container(
                          width: SizeConfig.screenWidth,
                          height: SizeConfig.screenHeight / 3.0,
                          child: Center(
                            child: SpinKitDualRing(
                              size: 40,
                              color: Colors.deepOrange,
                            ),
                          ),
                        )
                      : Column(
                          children: [
                            Align(
                              child: Padding(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text(
                                  "${enterprises.length} resultados encontrados",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: fontSizeMedium),
                                ),
                              ),
                              alignment: Alignment.centerLeft,
                            ),
                            for (int i = 0; i < enterprises.length; i++)
                              Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: enterpriseCard(
                                    enterpriseDto: enterprises[i], index: i),
                              )
                          ],
                        ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
