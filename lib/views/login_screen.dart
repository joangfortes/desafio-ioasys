import 'package:desafio_ioasys/components/appbar_clipper.dart';
import 'package:desafio_ioasys/components/login_screen_components.dart';
import 'package:desafio_ioasys/config/size.config.dart';
import 'package:desafio_ioasys/models/user.dto.dart';
import 'package:desafio_ioasys/services/auth.service.dart';
import 'package:desafio_ioasys/util/constraints.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

UserDto loggedUserDto;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();

  bool isLoading = false;
  bool areCredentialsWrong = false;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    bool isPopKeyboard =
        MediaQuery.of(context).viewInsets.bottom == 0 ? false : true;
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipPath(
            clipper: AppbarRounded(),
            child: Container(
              height: isPopKeyboard ? 150 : 250,
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/logo.png',
                    color: Colors.white,
                    scale: 3,
                  ),
                  isPopKeyboard
                      ? Container()
                      : Padding(
                          padding: EdgeInsets.only(top: 10.0),
                          child: Text(
                            'Seja bem vindo ao empresas!',
                            style: TextStyle(color: Colors.white, fontSize: 17),
                          ),
                        )
                ],
              ),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  stops: [
                    0.1,
                    0.3,
                    0.8,
                  ],
                  colors: [
                    Colors.redAccent[400],
                    Colors.pinkAccent[700],
                    Colors.deepPurple,
                  ],
                ),
              ),
            ),
          ),
          Container(
            height: SizeConfig.screenHeight / 1.5,
            child: Padding(
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Email"),
                  Padding(padding: EdgeInsets.only(bottom: 10)),
                  customTextField(emailTextEditingController,
                      areCredentialsWrong: areCredentialsWrong,
                      context: context),
                  Padding(padding: EdgeInsets.only(bottom: 10)),
                  Text("Senha"),
                  Padding(padding: EdgeInsets.only(bottom: 10)),
                  customTextField(passwordTextEditingController,
                      obscureText: true,
                      areCredentialsWrong: areCredentialsWrong,
                      context: context),
                  areCredentialsWrong
                      ? Row(
                          children: [
                            Expanded(child: Container()),
                            Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: Text(
                                "Credenciais incorretas",
                                style: TextStyle(color: Colors.red),
                              ),
                            ),
                          ],
                        )
                      : Container(),
                  Padding(padding: EdgeInsets.only(bottom: 50)),
                  Container(
                    width: SizeConfig.screenWidth,
                    height: SizeConfig.screenWidth / 8.0,
                    child: Padding(
                      padding: EdgeInsets.only(right: 10, left: 10),
                      child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.resolveWith<Color>(
                              (Set<MaterialState> states) {
                                if (states.contains(MaterialState.pressed))
                                  return isLoading
                                      ? Colors.grey.withOpacity(0.6)
                                      : Colors.red;
                                return isLoading
                                    ? Colors.grey.withOpacity(0.6)
                                    : Colors
                                        .pinkAccent; // Use the component's default.
                              },
                            ),
                          ),
                          child: isLoading
                              ? SpinKitDualRing(
                                  size: 40,
                                  color: Colors.white,
                                )
                              : Text(
                                  "ENTRAR",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: fontSizeBig),
                                ),
                          onPressed: isLoading
                              ? null
                              : () async {
                                  setState(() {
                                    isLoading = true;
                                  });
                                  await AuthServices()
                                      .makeLogin(emailTextEditingController,
                                          passwordTextEditingController,
                                          areCredentialsWrong:
                                              areCredentialsWrong)
                                      .then((isCredentialsWrong) => {
                                            areCredentialsWrong =
                                                isCredentialsWrong
                                          });
                                  setState(() {
                                    isLoading = false;
                                  });
                                }),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
