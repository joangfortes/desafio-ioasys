class ServerConfig {
  static const String dev_host = "https://empresas.ioasys.com.br";

  static const String api_version = "v1";

  static const String apiUrl = "$dev_host/api/$api_version";

  static String accessToken;

  static String uid;

  static String client;
}
